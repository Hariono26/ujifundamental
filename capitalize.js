// fungsi untuk mengubah setiap huruf pertama dari setiap kata
// output = "Aja Kaya Kue"

const capitalize = (string) => {
    let arr = string.split(" ")
    let firstCap = arr.map(item => {
        return item[0].toUpperCase() + item.slice(1)
    })
    return firstCap.join(" ")
}


let input = 'Aja kaya kue'
console.log(capitalize(input))

